import { Component, OnInit } from '@angular/core';
import { CONFIG } from './shared/app.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  productListUrl: string;
  addProductUrl: string;
  title = 'inventory-frontend';

  ngOnInit() {
    this.productListUrl = CONFIG.appRoutes.productList;
    this.addProductUrl = CONFIG.appRoutes.addProduct;

  }
}
