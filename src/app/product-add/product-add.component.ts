import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CONFIG } from '../shared/app.config';
import { Product } from '../shared/model/type';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {
  productFromGroup: FormGroup;
  productId: number = 0;
  constructor(
    private readonly productService: ProductService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly activeRoute: ActivatedRoute
  ) {
    this.productFromGroup = new FormGroup({
      id: new FormControl('0', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      price: new FormControl('', [Validators.required]),
      sellingPrice: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(res => {
      this.productId = res.id;
      if (this.productId > 0) {
        this.productService.getProduct(this.productId).subscribe(res => {
          this.setProduct(res);
        });
      }
    });



  }

  onSubmit() {
    if (this.productFromGroup.valid) {
      const product: Product = {
        addedOn: new Date().toISOString(),
        name: this.productFromGroup.value.name,
        price: this.productFromGroup.value.price,
        sellingPrice: this.productFromGroup.value.sellingPrice,
        description: this.productFromGroup.value.description,
        id: Number(this.productId)
      };

      if (this.productId > 0) {
        this.productService.updateProduct(product).subscribe(res => {
          this.snackBar.open('Product updated sucessfully!', 'Okay', { duration: 5000 });
          this.router.navigate([CONFIG.appRoutes.productList]);
        });
      } else {
        this.productService.addProduct(product).subscribe(res => {
          this.snackBar.open('Product added sucessfully!', 'Okay', { duration: 5000 });
          this.router.navigate([CONFIG.appRoutes.productList]);
        });
      }

    }
  }

  setProduct(product: Product) {
    this.productFromGroup.controls['id'].setValue(product.id);
    this.productFromGroup.controls['name'].setValue(product.name);
    this.productFromGroup.controls['price'].setValue(product.price);
    this.productFromGroup.controls['description'].setValue(product.description);
    this.productFromGroup.controls['sellingPrice'].setValue(product.sellingPrice);
  }

}
