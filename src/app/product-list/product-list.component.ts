import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CONFIG } from '../shared/app.config';
import { Product } from '../shared/model/type';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products: Product[];
  displayedColumns: string[] = ['id', 'name', 'description', 'price', 'sellingPrice', 'addedOn', 'actions'];
  dataSource;


  constructor(
    private readonly productService: ProductService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar
    ) {

  }

  ngOnInit(): void {
    this.showProductList();
  }

  showProductList() {
    this.productService.getProductList().subscribe(res => {
      this.products = res;
      this.dataSource = res;
    });
  }

  addProduct() {
    this.router.navigate([CONFIG.appRoutes.addProduct]);
  }

  editProduct(productId: number) {
    this.router.navigate([CONFIG.appRoutes.editProductUrl, productId]);
  }

  deleteProduct(productId: number) {
    this.productService.deleteProduct(productId).subscribe(res=>{
      this.snackBar.open("Product deleted successfull!", 'Okay', {duration:5000});
      this.showProductList();
    });
  }

}
