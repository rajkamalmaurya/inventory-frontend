export interface Product {
  id?: number;
  name: string;
  description: string;
  price: number;
  sellingPrice: number;
  addedOn: string;
}


export interface Config {
  appRoutes: AppRoutes

}

export interface AppRoutes {
  productList: string;
  addProduct: string;
  editProduct: string;
  editProductUrl: string;
}
