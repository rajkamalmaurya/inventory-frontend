import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../model/type';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private readonly httpClient: HttpClient) { }

  getProductList() : Observable<Product[]> {
    return this.httpClient.get<Product[]>(`${environment.backendServiceUrl}products`).pipe(delay(1000));
  }

  addProduct(product: Product) : Observable<Product> {
    return this.httpClient.post<Product>(`${environment.backendServiceUrl}products`, product).pipe(delay(1000));
  }

  deleteProduct(productId: number) : Observable<any> {
    return this.httpClient.delete<any>(`${environment.backendServiceUrl}products/${productId}`).pipe(delay(1000));
  }


  getProduct(productId: number) : Observable<Product> {
    return this.httpClient.get<Product>(`${environment.backendServiceUrl}products/${productId}`);
  }

  updateProduct(product: Product) : Observable<Product> {
    console.log(product);
    return this.httpClient.put<Product>(`${environment.backendServiceUrl}products/${product.id}`, product);
  }



}
