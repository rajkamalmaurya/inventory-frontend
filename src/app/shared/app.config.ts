import { Config } from "./model/type";

export const CONFIG: Config = {
  appRoutes: {
    addProduct: 'add-product',
    editProduct: 'edit-product/:id',
    editProductUrl: 'edit-product',
    productList: 'product-lists',

  }
}
