import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductListComponent } from './product-list/product-list.component';
import { CONFIG } from './shared/app.config';


const routes: Routes = [
  { path: CONFIG.appRoutes.addProduct, component: ProductAddComponent },
  { path: CONFIG.appRoutes.editProduct, component: ProductAddComponent },
  { path: CONFIG.appRoutes.productList, component: ProductListComponent },
  { path: '', pathMatch: 'full', redirectTo: CONFIG.appRoutes.productList },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
